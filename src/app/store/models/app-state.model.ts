import { ShoppingItem } from './shopping-item.model';
import { ShoppingState } from '../reducers/shopping.reducers';

export interface AppState {
  readonly shopping: ShoppingState
}